//version inicial
//webserver
var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//12-03: mongo
//peticiones post a travez de la api
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, ACcept");
    next();
});

var requestjson= require('request-json');
//--Movimientos
var  urlMovimientos = "https://api.mlab.com/api/1/databases/rarroyo/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var movimientosMlab = requestjson.createClient(urlMovimientos);
//--

//--Clientes
var  urlCliente = "https://api.mlab.com/api/1/databases/rarroyo/collections/clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlab = requestjson.createClient(urlCliente);
//--

//--Cuentas
var  urlCuentas = "https://api.mlab.com/api/1/databases/rarroyo/collections/cuentas?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var cuentasMlab = requestjson.createClient(urlCuentas);
//--

//--Tipo CAmbio
var  urlCAmbio = "http://e-consulta.sunat.gob.pe/cl-at-ittipcam/tcS01Alias";
var cambioMlab = requestjson.createClient(urlCAmbio);
//--

//12-03-fin

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req,res){
  //res.send("Hola Mundo nodejs");
  res.sendFile(path.join(__dirname,'index.html'));

})

app.post("/",function(req,res){
  res.send("Hemos recibido su petición post");

})

app.put("/",function(req,res){
  res.send("Hemos recibido su petición put cambiado");

})

app.delete("/",function(req,res){
  res.send("Hemos recibido su petición delete");

})

app.get("/v1/movimientos",function(req,res){
  res.sendFile(path.join(__dirname,'movimientosv1.json'));

})

//---movimientos
app.get('/movimientos',function(req, res){

  //err: errores, resm: respuestas
  movimientosMlab.get('',function(err,resM,body){
      if(err){
        console.log(err);
      }else{
        res.send(body);
      }

  });

});
app.post('/movimientos',function(req,res){
  //el json vendra en el body del request
  movimientosMlab.post('',req.body,function(err,resM,body){
    if(err){
      console.log(err);
    }else{
      res.send(body);
    }

  });

});

//LIstar movimientos de una cuenta
app.get('/movimientos/:cuenta',function(req, res){
const queryName = '&q={"cuentacargo":"' + req.params.cuenta + '"}';

var urlMovimientos2  = urlMovimientos + queryName
var movimientosMlab2 = requestjson.createClient(urlMovimientos2)
console.log(urlMovimientos2);
movimientosMlab2.get('',function(err,resM,body){

    if(err){
      console.log(urlMovimientos2);
      console.log(err);

    }else{
      var respuesta=body[0];
      console.log(body[0])
       if(undefined!=respuesta){
            console.log(urlMovimientos2);
            res.send(body);
          }
       else{
            res.send(404,{"msg":"No existen movimientos"});
            //res.send(body);
            console.log(urlMovimientos2);
          }
     }
});

});

//Validar saldo disponible
app.get('/saldocta/:cuenta&:importe&:abono',function(req, res){
  const queryName1 = '&q={"codigocuenta":"'+ req.params.cuenta+'"}';

  var abono   =   req.params.abono  ;
  var importe =   req.params.importe;

  var urlCuenta2  = urlCuentas + queryName1
  var cuentasMlab = requestjson.createClient(urlCuenta2)

  cuentasMlab.get('',function(err,resM,body){
      if(err){
        res.send(404,{"msg":"Error en procesar"});

      }else{
          var respuesta=body[0];

           if(undefined!=respuesta){
                     res.send(body);
                 }
           else{
             res.send(404,{"msg":"Error en procesar"});

              console.log(urlCuenta2);
           }
         }

  });

});

//Ingresar Movimientos
app.post('/ingresarMov',function(req,res){

 movimientosMlab.post('',req.body,function(err,resM,body){
   if(err){
     console.log(err);
   }else{
     console.log(movimientosMlab);

     var respuesta=body[0];
      if(undefined!=respuesta){
            res.send(404,{"msg":"incorrecto"});
      }
      else{
        res.send(201,{"msg":"Movimiento correcto"});
      }
   }
 });
});

app.get('/movimientos/:cuenta&:fechaoperacion',function(req, res){

const queryName1 = '&q={"cuentacargo":"'+ req.params.cuenta+'",';
const queryName2 = '"cuentaabono":"'     + req.params.documento +'",';
const queryName3 = '"importe":"'+ req.params.fechaoperacion+'"}';
const queryName  = queryName1 + queryName2

var urlMovimientos3 = urlMovimientos + queryName
var movimientosMlab1 = requestjson.createClient(urlMovimientos3)
  //err: errores, resm: respuestas
  movimientosMlab1.get('',function(err,resM,body){
      if(err){
        console.log(urlMovimientos3);
        console.log(err);
      }else{
        console.log(urlMovimientos3);
        res.send(body);
      }

  });

});

//**4.MODIFICACION DEL cuenta***
/*
*/
var  urlCuentax="https://api.mlab.com/api/1/databases/rarroyo/collections/cuentas";
var apikeyx="apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var  urlCuentas = "https://api.mlab.com/api/1/databases/rarroyo/collections/cuentas?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

//--
app.put('/updateCuenta/:codigocuenta',function (req, res) {
  console.log("entro");
  var codigocuenta = req.params.codigocuenta;

  let userBody = req.body;
//  var queryString = 'q={"_codigocuenta":{"$ocodigocuenta":"' + codigocuenta + '"}}&';

  const queryString = '&q={"codigocuenta":"'+ req.params.codigocuenta +'"}';

  console.log(queryString);

var urlCuentas2 = urlCuentas + queryString

    var cuentasMlabx = requestjson.createClient(urlCuentax);
    console.log(urlClientex + '?' + queryString + apikeyx);

    var cuentasMlab = requestjson.createClient(urlCuentas2);
    console.log(urlCuentas + queryString);


  //var cuentaMlab = requestjson.createClient(urlCuentax);

  //cuentasMlab.get('?' + queryString + apikeyx, function(err, resM, body){
  cuentasMlab.get('', function(err, resM, body){
      let response = body[0];
      console.log(body);

      //Actualizo los campos del usuario
      const updatedUser = {
        "saldo" : req.body.importe
      };

    //  var cambio = '{"$set":saldo":' 2000 +'}';

  //  console.log(urlCuentax+ '/' + response._codigocuenta.$ocodigocuenta + '?' + apikeyx);

      //Llamo al put de mlab.
      //cuentaMlab.put(urlCuentax + '/' + response._codigocuenta.$ocodigocuenta + '?' + apikeyx,
      //updatedUser, function(err, resM, body){
      cuentasMlab.put('', JSON.parse(cambio), function(err, resM, body){

          var response = {};
          console.log(err);
          if(err) {
              response = {
                "msg" : "Error actualizando cuenta."
              }
              res.status(500);
          } else {
            if(body.n <= 0) {
              response = body;
              console.log(body.n);

            } else {
                console.log(body.n);
              response = {
                "msg" : "Cuenta actualizado correctamente."

              }
              //res.status(404);
            }
          }
          res.send(response);
        });
    });
});
//12-03 fin
//10-04


//---cuentas
app.get('/cuentas',function(req, res){

  //err: errores, resm: respuestas
  cuentasMlab.get('',function(err,resM,body){
      if(err){
        console.log(err);
      }else{
         var respuesta=body[0];
         if(undefined!=respuesta){

                  res.send(body);

               }
         else{
           res.send(404,{"msg":"Credenciales incorrecta"});
         }
       }

  });

});

app.post('/cuentas',function(req,res){
  //el json vendra en el body del request
  cuentasMlab.post('',req.body,function(err,resM,body){
    if(err){
      console.log(err);
    }else{
      res.send(body);
    }

  });

});

//buscar x tipo numero documento clave
app.get('/cuentas/:idcliente',function(req, res){
const queryName = '&q={"idcliente":"'+ req.params.idcliente +'"}';

var urlCuentas2 = urlCuentas + queryName
var clienteMlab = requestjson.createClient(urlCuentas2)
  //err: errores, resm: respuestas
  clienteMlab.get('',function(err,resM,body){
      if(err){
        res.send(404,{"msg":"Credenciales incorrecta"});
        //res.send({ hello: 'world' });
        //console.log(urlCuentas2);
        //console.log(err);
      }else{
        //console.log(urlCuentas2);

        var respuesta=body[0];
         if(undefined!=respuesta){
              //   res.send(201,{"msg":"Listado de cuentas"})
              //    console.log(urlCuentas2);
              //    res.send(201,body);
                  res.send(body);
              //    console.log(body);
              //    console.log(body[0].codigocuenta);
              //      console.log("hola");
               }
         else{
           res.send(404,{"msg":"No existen cuentas"});
           //console.log(urlCuentas2);
         }
       }
  });
});

//**1. LOGIN****

app.get('/clientes',function(req, res){

  //err: errores, resm: respuestas
  clienteMlab.get('',function(err,resM,body){
      if(err){
        console.log(err);
      }else{
        res.send(body);
      }

  });

});
//buscar x ID
app.get('/clientes/:idcliente',function(req, res){
const queryName='&q={"idcliente":'+req.params.idcliente+'}';

var urlClientes2 = urlCliente + queryName
var clienteMlab = requestjson.createClient(urlClientes2)
  //err: errores, resm: respuestas
  clienteMlab.get('',function(err,resM,body){
      if(err){
        console.log(urlClientes2);
        console.log(err);
      }else{
        console.log(urlClientes2);

        var respuesta=body[0];
         if(undefined!=respuesta){
                 //res.send(201,{"msg":"Login correcto"})
                   res.send(body);
               }
         else{
           res.send(404,{"msg":"Credenciales incorrecta"});
         }
       }

  });
});

//buscar x ID
app.get('/constipocambio',function(req, res){

  //err: errores, resm: respuestas
  cambioMlab.get('',function(err,resM,body){
      if(err){
        console.log(cambioMlab);
        console.log(err);
      }else{
        console.log(cambioMlab);

        var respuesta=body[0];
         if(undefined!=respuesta){
                 //res.send(201,{"msg":"Login correcto"})
                   res.send(body);
                     console.log(body);
               }
         else{
           res.send(404,{"msg":"Credenciales incorrecta"});
             console.log(body);
         }
       }

  });
});


//buscar x tipo numero documento clave
app.get('/clienteLogin/:tipo&:documento&:clave&:situacion',function(req, res){
const queryName1 = '&q={"tipodocumento":"'+ req.params.tipo +'",';
const queryName2 = '"numdocumento":"'     + req.params.documento +'",';
const queryName3 = '"claveweb":"'   + req.params.clave +'",';
const queryName4 = '"situacion":"'+ req.params.situacion +'"}';
const queryName  = queryName1 + queryName2 + queryName3 + queryName4

var urlClientes3 = urlCliente + queryName
var clienteMlab = requestjson.createClient(urlClientes3)
 //err: errores, resm: respuestas
 clienteMlab.get('',function(err,resM,body){
     if(err){
       console.log(err);
     }else{
       console.log(urlClientes3);

       var respuesta=body[0];
        if(undefined!=respuesta){
                //res.send(201,{"msg":"Login correcto"})
                res.send(body);
              }
        else{
          res.send(404,{"msg":"Credenciales incorrecta"});
        }
      }
 });
});

//**2. MANTENIMIENTO DE CLIENTES****
//buscar x tipo numero documento clave
app.get('/clienteConsulta/:tipo&:documento&:situacion',function(req, res){
const queryName1 = '&q={"tipodocumento":"'+ req.params.tipo+'",';
const queryName2 = '"numdocumento":"'     + req.params.documento+'",';
const queryName3 = '"situacion":"'+ req.params.situacion+'"}';
const queryName  = queryName1 + queryName2 + queryName3

var urlClientes3 = urlCliente + queryName
var clienteMlab = requestjson.createClient(urlClientes3)
 clienteMlab.get('',function(err,resM,body){
     if(err){
       console.log(err);
     }else{
       console.log(urlClientes3);

       var respuesta=body[0];
        if(undefined!=respuesta){
               res.send(404,{"msg":"Usuario ya existe"});
               //res.send(body);
               }
        else{
          res.send(201,{"msg":"Usuario no existe"})
        }
      }
 });
});

//ingresar Cliente
app.post('/clientesinsert',function(req,res){
 //el json vendra en el body del request
 clienteMlab.post('',req.body,function(err,resM,body){
   if(err){
     console.log(err);
   }else{
     var respuesta=body[0];
      if(undefined!=respuesta){
             res.send(404,{"msg":"incorrecto"});
             res.send(body);
            }
      else{
           res.send(201,{"msg":"Cliente correcto"});
           console.log({"msg":"Cliente correcto"});
      }
   }
 });
});



//**3. APERTURAR CUENTA****

app.post('/aperturarCuenta',function(req,res){
 //el json vendra en el body del request
 cuentasMlab.post('',req.body,function(err,resM,body){
   if(err){
     console.log(err);
   }else{
     var respuesta=body[0];
      if(undefined!=respuesta){
             res.send(404,{"msg":"incorrecto"});
            }
      else{
           res.send(201,{"msg":"correcto"});
         }
   }
 });
});

//**4.MODIFICACION DEL CLIENTE***
/*
*/
var  urlClientex="https://api.mlab.com/api/1/databases/rarroyo/collections/clientes";
var apikey="apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

app.put('/updateCliente/:id',function (req, res) {
 console.log("entra????");
  var id = req.params.id;
  console.log(id);
  let userBody = req.body;
  var queryString = 'q={"_id":{"$oid":"' + id + '"}}&';
  console.log(queryString);

  var clienteMlab = requestjson.createClient(urlClientex);
  console.log(urlClientex + '?' + queryString + apikey);

  clienteMlab.get('?' + queryString + apikey, function(err, resM, body){

      let response = body[0];
      console.log(body);

      //Actualizo los campos del usuario
      const updatedUser = {
        "tipodocumento" : req.body.tipodocumento,
        "numdocumento" : req.body.numdocumento,
        "nombres" : req.body.nombres,
        "apepat" : req.body.apepat,
        "apemat" : req.body.apemat,
        "email" : req.body.email,
        "claveweb" : req.body.claveweb,
        "situacion" : req.body.situacion
      };

    console.log(urlClientex + '/' + response._id.$oid + '?' + apikey);

      //Llamo al put de mlab.
      clienteMlab.put(urlClientex + '/' + response._id.$oid + '?' + apikey, updatedUser, function(err, resM, body){
          var response = {};
          console.log(err);
          if(err) {
              response = {
                "msg" : "Error actualizando usuario."
              }
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Usuario actualizado correctamente."
              }
              //res.status(404);
            }
          }
          res.send(response);
        });
    });
});
